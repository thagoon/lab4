/*
 * XMLFileWriter.java (DOM)
 * This java application that it produces an XML File "quotes.xml"
 *
 * @author Thagoon Bunlue
 * id : 533040442-3
 */

package coe.kku.webservice;

import java.io.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

public class XmlWriter {

    public static void main(String argv[]) {

        File file = new File("quotes.xml");

        try {

            DocumentBuilderFactory builderFactory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();

            // Create quotes to root element
            Element rootElement = doc.createElement("quotes");
            doc.appendChild(rootElement);

            // Create first quote element
            Element quote1E = doc.createElement("quote");
            rootElement.appendChild(quote1E);

            Element word1E = doc.createElement("word");
            word1E.appendChild(doc.createTextNode("Time is more value than money. You can get more money, but you cannot get more time"));
            quote1E.appendChild(word1E);

            Element by1E = doc.createElement("by");
            by1E.appendChild(doc.createTextNode("Jim Rohn"));
            quote1E.appendChild(by1E);

            // Create second quote element
            Element quote2E = doc.createElement("quote");
            rootElement.appendChild(quote2E);

            Element word2E = doc.createElement("word");
            word2E.appendChild(doc.createTextNode("เมื่อทำอะไรสำเร็จ แม้จะเป็นเก้าเล้กๆของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง"));
            quote2E.appendChild(word2E);

            Element by2E = doc.createElement("by");
            by2E.appendChild(doc.createTextNode("ว. วชิรเมธี"));
            quote2E.appendChild(by2E);

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(file);

            trans.transform(source, result);

            System.out.println("Create quotes.xml Success!!!");

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
